# AWS Lambda Function

## Function Introduction: Polars
Polars Lambda is a Rust-based AWS Lambda function designed for high-performance data processing using the Polars library. This project demonstrates how to filter and aggregate data from a CSV dataset in a serverless environment, showcasing the power of Rust and Polars for data-intensive applications.

## Features
- Utilizes the Polars library for efficient data manipulation.
- Designed to run as an AWS Lambda function for scalable, serverless data processing.
- Filters and aggregates data based on dynamic input parameters.

## Requirements
- Rust 2021 Edition
- AWS Lambda runtime for Rust
- Tokio for async runtime

## Setup
To get started with Polars Lambda, ensure you have Rust and Cargo installed. Then, follow these steps:

1. Clone the repository to your local machine.
2. Navigate to the project directory and build the project using Cargo:

```
cargo build --release
```
3. Deploy the built binary to AWS Lambda, setting up the appropriate triggers and permissions as required by your application.

## Usage
The Lambda function expects a JSON payload with a filter key, specifying the minimum value for filtering the dataset. The response includes a summarized view of the data, aggregating values based on the filter criteria.

Example request payload:

```
{
  "filter": 5
}
```

This will return aggregated data for entries with a value greater than 5.

## Usage Example

This function has been successfully deployed to AWS Lambda in the following invoke URL: https://rt3v8c7a4k.execute-api.us-east-1.amazonaws.com/test/. The function being deployed to is under the name ``filter``.
![Function overview](2.png)

We used [mtcars](https://gist.github.com/seankross/a412dfbd88b3db70b74b) dataset to do the test and use 
``{
  "filter": 5
}``


Below image indicating the test is success:
![Function overview](WechatIMG793.png)

